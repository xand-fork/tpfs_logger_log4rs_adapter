#[cfg(test)]
use chrono::NaiveDateTime;
use chrono::{DateTime, Utc};
use log::Record;
use log4rs::encode::{Encode, Write};
use serde::Serialize;
use serde_json::value::RawValue;
use serde_json::Value;
use std::{error::Error, thread};
use tpfs_logger_port::JsonEncodedLogMsg;

/// This is duplicative of the `JsonEncoder` built into Log4rs, but that didn't have many options
/// to configure what was logged.
#[derive(Debug)]
pub struct JsonEncoder;

impl Encode for JsonEncoder {
    fn encode(
        &self,
        w: &mut dyn Write,
        record: &Record<'_>,
    ) -> Result<(), Box<dyn Error + Sync + Send>> {
        let thread = thread::current();
        // TODO: This is somewhat wasteful (RawValue helps), but the problem is since our port
        //  library can only operate in terms of returning records, the `Arguments` structure is
        //  all we have to work with and we can't get the serializeable value here directly, so we
        //  have to deserialize it and then reserialize it. This will go away when the K/V pairs
        //  feature of `log` is stabilized. The deserializtion is relatively cheap with `RawValue`
        //  https://www.pivotaltracker.com/story/show/171390974
        let msg_val = RawValue::from_string(format!("{}", record.args())).map_or_else(
            |_| {
                // Not issued by our library (logs from places we don't own) or otherwise invalid json,
                // so turn it into a json string.
                let strval = Value::String(format!("{}", record.args()));
                RawValue::from_string(
                    serde_json::to_string(&strval).expect("json good by construction"),
                )
                .expect("json good by construction")
            },
            |value| value,
        );

        let message = JsonEncodedLogMsg {
            time: now(),
            msg: &msg_val,
            level: record.level(),
            module_path: record.module_path(),
            file: record.file(),
            line: record.line(),
            target: record.target(),
            thread: thread.name(),
        };
        message.serialize(&mut serde_json::Serializer::new(&mut *w))?;
        w.write_all(NEWLINE.as_bytes())?;
        Ok(())
    }
}

#[allow(dead_code)]
#[cfg(windows)]
const NEWLINE: &str = "\r\n";
#[allow(dead_code)]
#[cfg(not(windows))]
const NEWLINE: &str = "\n";

// Simple way to get a consistent timestamp during tests.

#[allow(dead_code)]
#[cfg(not(test))]
fn now() -> DateTime<Utc> {
    Utc::now()
}

#[allow(dead_code)]
#[cfg(test)]
fn now() -> DateTime<Utc> {
    DateTime::<Utc>::from_utc(
        NaiveDateTime::from_timestamp_opt(0, 0).expect("This should never panic."),
        Utc,
    )
}
